# Daciuk

a minimal, deterministic, acyclic finite state automaton\
reference:\
Daciuk et al.\
Incremental Construction of Minimal Acyclic Finite-State Automata\
https://www.aclweb.org/anthology/J00-1002.pdf
