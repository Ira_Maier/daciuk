import importlib

class Daciuk:
    def __init__(self,user_input):
        user_input = user_input.strip()
        self.input = sorted(user_input.split(' '))
        self.start = Node(0)
        self.common_prefix = [self.start] #list of Nodes
        self.register = []
        self.last_state = self.common_prefix[-1]
        self.current_suffix = ''
        self.node_counter = 0
        self.main()

    def main(self):
        for word in self.input:
            common_prefix = self.set_common_prefix(word)
            current_suffix = word[len(common_prefix):]
            if self.last_state.transitions:
                self.replace_or_register(self.last_state)
            self.add_suffix(self.last_state, current_suffix)
        self.replace_or_register(self.start)

    def add(self, user_input):
        self.input.extend(user_input.split(' '))
        self.main()

    def read_input(self):
        for word in self.input:
            self.common_prefix(word)

    def print(self, node):
        '''
        depth first search
        :param node:
        :return: list of words in automaton
        '''
        words = []
        def inner(node, word):
            if node.transitions == []:
                word = ''
            else:
                for i in node.transitions.items():
                    word_new = word
                    word_new += i[0]
                    for v in i[1]:
                        if v.final == True:
                            words.append(word_new)
                        inner(v, word_new)
        inner(node, '')
        return words

    def set_common_prefix(self, word):
        '''
        appends nodes to common prefix
        :param word: current word
        :return:
        '''
        old_common_prefix = self.read_node_list_chars(self.common_prefix) #common prefix before it is changed by the new word
        new_common_prefix = self.get_prefix(word, old_common_prefix)
        old_node = self.start
        if new_common_prefix == '':
            #put whole word in common prefix
            for c in word:
                self.node_counter += 1
                new_node = Node(self.node_counter)
                self.common_prefix[-1].transitions[c] = [new_node]
                new_node.parent = old_node
                self.common_prefix.append(new_node)
                old_node = new_node
            self.common_prefix[-1].final = True
            self.last_state = self.common_prefix[-1]
            new_common_prefix = word

        elif len(new_common_prefix) < len(old_common_prefix):
            #shrink common prefix
            del self.common_prefix[len(new_common_prefix)+1:]
            self.last_state = self.common_prefix[-1]
        return new_common_prefix

    def get_prefix(self, word1, word2):
        word_back = ''
        if len(word1) <= len(word2):
            min_w, max_w = word1, word2
        else:
            min_w, max_w = word2, word1
        for i in enumerate(min_w):
            if i[1] == max_w[i[0]]:
                word_back += i[1]
            if i[1] != max_w[i[0]]:
                return word_back
        return word_back

    def add_suffix(self, last_state, current_suffix):
        old_node = last_state
        if current_suffix:
            self.node_counter += 1
            new_node = Node(self.node_counter)
            new_node.parent = old_node
            if len(current_suffix) == 1:
                new_node.final = True
            if current_suffix[0] in last_state.transitions:
                last_state.transitions[current_suffix[0]].append(new_node)
            else:
                last_state.transitions[current_suffix[0]] = [new_node]
            return self.add_suffix(new_node, current_suffix[1:])

    def replace_or_register(self, last_state):
        child = list(last_state.transitions.items())[-1][1][0] #child of last state
        char2child = list(last_state.transitions.items())[-1][0]
        if child.transitions:
            self.replace_or_register(child)
        if not self.register: self.register.append(child)
        add2reg = True
        for r in self.register:
            if self.print(r) == self.print(child) and r.final == child.final:
                last_state.transitions[char2child].append(r)
                last_state.transitions[char2child].remove(child)
                add2reg = False
        if add2reg == True: self.register.append(child)

    def read_node_list_chars(self, node_list):
        '''

        :param node_list: list of Nodes objects
        :return: list with outgoing chars of Nodes instead of Node objects
        '''
        word = ''
        for n in node_list:
            if n.transitions:
                for c, n2 in n.transitions.items():
                    if n2[0] in node_list:
                        word += c
        return word

    def read_node_list_nums(self, node_list):
        '''

        :param node_list: list of Nodes objects
        :return: list with Node nums instead of Node objects
        '''
        word = ''
        for n in node_list:
            if n.transitions:
                for c, n2 in n.transitions.items():
                    if n2[0] in node_list:
                        word += str(n2[0].num)+' '
        return word

    def get_triples(self):
        '''

        :return: list of triples with start node, char, target node e. g. '0 e 1'
        '''
        triples = []
        def inner(node):
            start = node.num
            for i in node.transitions.items():
                label = i[0]
                for v in i[1]:
                    target = v.num
                    triples.append((start, label, target, v.final))
                    inner(v)
        inner(self.start)
        return triples

class Node:
    def __init__(self, num):
        self.num = num
        self.parent = None
        self.transitions = {} #keys: chars, values: Node objects
        self.final = False

if __name__ == '__main__':
    importlib.invalidate_caches()
    input_str = ' eine elster erster ester etne einnehmen einer ' #words to put in the automaton
    my_dac = Daciuk(input_str) #build automaton
    del my_dac.register #register is not longer needed because all Nodes are saved in the automaton
    print(my_dac.print(my_dac.start)) #print out words from automaton


